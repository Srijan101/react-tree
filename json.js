export default {
	timestamp: '19/06/2020 17:00',
	name: 'Agora,Inc (Cayman Islands)',
	children: [{
		name: 'Agora,Inc',
		children: []
	}, {
		name: 'Agora IO Honkong Limited (honkong)',
		children: [{
			name: 'Dayin Network Technology Co., LTD(PRC)("WFOE")',
			children: [{
				name: 'Shanghai Zhaoyan Network Technology Co ltd(PCR) ("VIE")',
				children: []
			}]
		}]
	}, {
		name: 'Agora Lab Inc (U.S)',
		children: [{
			name: 'Agora.IO Ltd',
			children: []
		}]
	}]
};